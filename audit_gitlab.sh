#!/bin/bash

# Requires env vars:
#
# * gitlab_token=<gitlab access token with api scope>
# * allowed_users_file=<path to file with newline separated list of allowed usernames>

##### REST implementation

if [[ -z "$gitlab_token" || -z "$allowed_users_file" ]]; then
  echo 'ERROR: Script requires two env vars: gitlab_token and allowed_users_file.' >&2
  exit 1
fi

SAVEIFS=$IFS
IFS=$(echo -en "\n\b")

pagination_wrapper () {
  query="${1}"
  page=1
  rate_limit=100
  while true; do
    if [[ $page -gt $rate_limit ]]; then
      echo "ERROR: rate_limit is set to ${rate_limit}. If you actually have more pages, adjust this script." >&2
      break
    fi
    # This is sneaky... it builds the function name using the git_provider
    # variable, then executes it with eval.    
    output=$(eval "curl -s -H \"Private-Token: ${gitlab_token}\" -s \"https://gitlab.com/api${query}?page=${page}\"")

    case $output in
      '[]') break;;
      '{}') break;;
      *) echo "$output";;
    esac
    ((page++))
  done
}

get_gl_groups () {
  pagination_wrapper '/v4/groups' | \
  jq -cr '.[] | {"id":.id,"web_url":.web_url}' | \
  sort
}

# Takes Gitlab group ID as argument.
get_gl_group_members () {
  pagination_wrapper "/v4/groups/${1}/members" | \
  jq -cr '.[] | {"username":.username,"name":.name,"access_level":.access_level}' | \
  sort
}

for gl_group in $(get_gl_groups); do
  gl_group_id=$(echo $gl_group | jq -r .id)
  gl_group_web_url=$(echo $gl_group | jq -r .web_url)
  output="${gl_group_web_url}\n"

  # Reset found_user before entering loop again.
  found_user=false

  for gl_member in $(get_gl_group_members "$gl_group_id"); do
    gl_member_username=$(echo $gl_member | jq -r .username)
    gl_member_name=$(echo $gl_member | jq -r .name)
    gl_member_access_level=$(echo $gl_member | jq -r .access_level)
    if ! grep -qE "^${gl_member_username}$" "${allowed_users_file}"; then
      output+="* ${gl_member_username} (${gl_member_access_level}) aka \"${gl_member_name}\" is not authorized\n"
      found_user=true
    fi
  done
  if $found_user; then
    echo -e "$output"
  fi
done

exit

##### Attempting to refactor to GraphQL - I don't know what the hell I'm doing.

q_currentUser='{"query":"query{currentUser{name}}"}'

q_group='{"query":"query{group(fullPath:\"gitlab-org\"){id name projects{nodes{name}}}}"}'

set -x
#curl -s 'https://gitlab.com/api/graphql' --header "Authorization: Bearer $gitlab_token" --header "Content-Type: application/json" --request POST --data '{"query": "query {currentUser {name}}"}' | jq .

echo "$q_currentUser" | jq -cr .

curl -s 'https://gitlab.com/api/graphql' --header "Authorization: Bearer $gitlab_token" --header "Content-Type: application/json" --request POST --data "$q_currentUser" | jq .

echo "$q_group" | jq -cr .

curl -s 'https://gitlab.com/api/graphql' --header "Authorization: Bearer $gitlab_token" --header "Content-Type: application/json" --request POST --data "$q_group" | jq .
